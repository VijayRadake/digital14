//
//  APIEndpoints.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation

struct APIEndpoints
{
    private static let clientId = "MjQ2OTIzMDV8MTYzODI1NDcxMC42MDkzNzk1"
    
    static let baseUrl = "https://api.seatgeek.com/2/"
    
    static func eventsUrlString(searchKey: String?) -> String
    {
        if let encodedQuery = searchKey?.urlEncoding(), !encodedQuery.isEmpty {
            return baseUrl + "events?client_id=\(APIEndpoints.clientId)&q=\(encodedQuery)"
        }
        return baseUrl + "events?client_id=\(APIEndpoints.clientId)"
    }
}
