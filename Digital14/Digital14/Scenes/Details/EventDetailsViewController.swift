//
//  EventDetailsViewController.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol EventDetailsDisplayLogic: AnyObject
{
    func displayEvent(event: EventViewModel?)
}

class EventDetailsViewController: UIViewController, EventDetailsDisplayLogic
{
    var interactor: EventDetailsBusinessLogic?
    var router: (NSObjectProtocol & EventDetailsRoutingLogic & EventDetailsDataPassing)?
    var completionHandler: (() -> Void)?
    
    // MARK: IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var eventPlaceLabel: UILabel!
    
    var selectedEvent: EventViewModel?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = EventDetailsInteractor()
        let presenter = EventDetailsPresenter()
        let router = EventDetailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
        
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        interactor?.getEvent()
    }
    
    func displayEvent(event: EventViewModel?)
    {
        selectedEvent = event
        guard let event = selectedEvent else {
            return
        }
        eventImageView.setImage(url: event.eventImageUrl, placeholder: UIImage(named: "Placeholder"))
        eventImageView.roundCorners()
        titleLabel.text = event.eventTitle
        eventDateLabel.text = event.eventDateTime
        eventPlaceLabel.text = event.eventLocation
        favoriteButton.isSelected = event.isFavorites
    }
}

extension EventDetailsViewController
{
    @IBAction func backButtonClicked(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
        completionHandler?()
    }
    
    @IBAction func favoriteButtonClicked(_ sender: Any)
    {
        interactor?.favoriteButtonAction()
    }
}
