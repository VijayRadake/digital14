//
//  EventsListPresenter.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol EventsListPresentationLogic
{
    func presentEvents(response: EventsList.fetch.Response)
    func present(error: Error?)
    func presentSelectedEventWithUpdatedStatus(event: EventViewModel)
}

class EventsListPresenter: EventsListPresentationLogic
{
    weak var viewController: EventsListDisplayLogic?
    
    // MARK: Do something
    
    func presentEvents(response: EventsList.fetch.Response)
    {
        let viewModel = EventsList.fetch.ViewModel(response: response)
        viewController?.displayEvents(viewModel: viewModel)
    }
    
    func present(error: Error?)
    {
        
    }
    
    func presentSelectedEventWithUpdatedStatus(event: EventViewModel)
    {
        viewController?.displayFavoriteForSelectedEvent(event: event)
    }
}
