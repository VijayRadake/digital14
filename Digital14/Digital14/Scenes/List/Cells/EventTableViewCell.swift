//
//  EventTableViewCell.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import UIKit

class EventTableViewCell: UITableViewCell
{
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventPlaceLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        eventImageView.roundCorners()
    }
    
    func prepareCell(viewModel: EventViewModel)
    {
        eventTitleLabel.text = viewModel.eventTitle
        eventPlaceLabel.text = viewModel.eventLocation
        eventDateLabel.text = viewModel.eventDateTime
        eventImageView.setImage(url: viewModel.eventImageUrl, placeholder: UIImage(named: "Placeholder"))
        favoriteImageView.isHidden = !viewModel.isFavorites
    }
}
