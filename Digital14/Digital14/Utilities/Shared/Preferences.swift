//
//  Preferences.swift
//  Digital14
//
//  Created by Vijay Radake on 02/12/21.
//

import Foundation

final class Preferences
{
    static let shared = Preferences()
    private init() {
        favoritesEventsId = UserDefaults.standard.value(forKey: "FavoritesEvents") as? [Int] ?? [Int]()
    }
    private var favoritesEventsId: [Int]
    
    func setFavorites(eventId: Int)
    {
        if let index = favoritesEventsId.firstIndex(of: eventId) {
            favoritesEventsId.remove(at: index)
        } else {
            favoritesEventsId.append(eventId)
        }
        UserDefaults.standard.set(favoritesEventsId, forKey: "FavoritesEvents")
        UserDefaults.standard.synchronize()
    }
    
    func isfavorites(eventId: Int) -> Bool
    {
        return favoritesEventsId.contains(eventId)
    }
}
