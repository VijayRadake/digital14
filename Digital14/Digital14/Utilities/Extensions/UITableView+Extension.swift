//
//  UITableViewCell+Extension.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation
import UIKit

protocol ReusableView: AnyObject
{
    static var reuseIdentifier: String { get }
}

extension ReusableView where Self: UIView
{
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReusableView { }

extension UITableView
{
    func dequeueCell<T: UITableViewCell>() -> T
    {
        return (dequeueReusableCell(withIdentifier: T.reuseIdentifier) as? T)!
    }
}
