//
//  String+Extension.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation

extension String
{
    func urlEncoding() -> String
    {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
}
