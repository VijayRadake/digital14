//
//  ViewController+Extension.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation
import UIKit

extension UIViewController
{
    func displayAlertWith(title: String?, message: String?)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
