//
//  UIView+Extension.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation
import UIKit

extension UIView
{
    func roundCorners(radius: CGFloat = 6.0)
    {
        clipsToBounds = true
        layer.cornerRadius = radius
    }
}
