//
//  UIImageView+Extension.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation
import UIKit
import AlamofireImage
import ObjectiveC.runtime

var activityIndicatorAssociationKey = "activityIndicatorAssociationKey"

extension UIImageView
{
    
    
    func setImage(url: String?, placeholder: UIImage? = nil)
    {
        guard let urlString = url, let imageUrl = URL(string: urlString) else {
            image = nil
            return
        }
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.center = center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true

        af.setImage(withURL: imageUrl, placeholderImage: placeholder, completion:  { _ in
            activityIndicator.stopAnimating()
        })
    }
}
