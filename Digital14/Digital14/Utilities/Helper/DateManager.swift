//
//  DateManager.swift
//  Digital14
//
//  Created by Vijay Radake on 30/11/21.
//

import Foundation

struct DateManager
{
    private let utcDateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    private let displayDateFormat = "E, d MMM yyyy h:mm a"
    
    func displayDate(inputDate: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = utcDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let utcDate = dateFormatter.date(from: inputDate) else {
            return ""
        }
        dateFormatter.dateFormat = displayDateFormat
        return dateFormatter.string(from: utcDate)
    }
    
    
}
